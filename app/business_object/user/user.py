from typing import Optional
from pydantic import BaseModel


class User(BaseModel):
    id: Optional[int] = None
    username: str
    password: str

    def __init__(self,id:Optional[int],username:str,password:str):
        super().__init__(
        id = id,
        username = username,
        password=password
        )