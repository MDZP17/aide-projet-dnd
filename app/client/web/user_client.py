
from app.business_object.user.user import User
import requests
import os

from app.client.configuration import Configuration
from app.web_service.service.user_service import UserService
from typing import List

class UserClient:

    @staticmethod
    def createUser(user: User) :
        pass

    @staticmethod
    def authenticate_and_get_user(username: str, password: str) :
        configuration = Configuration()
        api_url = configuration.getApiUrl()
        api_dest = str.format("{}/users/{}",api_url,username)
        userAsDict = requests.get(api_dest)
        return userAsDict

    @staticmethod
    def updateUser(user: User)-> None:
        configuration = Configuration()
        api_url = configuration.getApiUrl()
        api_dest = str.format("{}/users/{}",api_url,username)
        userAsDict = requests.post(api_dest)
        
    @staticmethod
    def deleteUser(user:User):
        pass

    @staticmethod
    def getAllUsers() :
        configuration = Configuration()
        api_url = configuration.getApiUrl()
        api_dest = str.format("{}/users",api_url)
        usersAsDict = requests.get(api_dest)
        usersAsDict = usersAsDict.json()
        users = []
        for userAsDict in usersAsDict:
            users.append(UserClient.userDictToUser(userAsDict))
        return users

    @staticmethod
    def userDictToUser(user_as_dict: dict) -> User:
        return User(user_as_dict["id"],user_as_dict["username"],user_as_dict["password"])