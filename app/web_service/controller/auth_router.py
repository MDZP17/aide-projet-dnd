from fastapi import APIRouter
from app.business_object.user.user import User
from app.web_service.service.user_service import UserService

router = APIRouter()


@router.post("/users/", tags=["users"])
def create_user(user: User):
    return UserService.createUser(user)

@router.get("/users/", tags=["users"])
def get_all_users():
    return UserService.get_all_users()

@router.put("/users/{user_name}", tags=["users"])
def update_user(user_name: str, user: User):
    return UserService.updateUser(user_name, user)


@router.get("/users/{user_name}", tags=["users"])
def get_user(user_name: str):
    return UserService.getUser(user_name)
